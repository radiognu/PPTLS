function ready(fn) {
    if (document.readyState != 'loading') {
        fn();
    } else {
        document.addEventListener('DOMContentLoaded', fn);
    }
}

ready(function () {
    var options = ['papel', 'piedra', 'lagarto', 'spock', 'tijeras'],
        results = [
            [' se junta con ', ' tapa a ', ' es devorado por ',
                ' desautoriza a ', ' es cortado por '
            ],
            [' es tapada por ', ' se apila con ', ' aplasta a ',
                ' es vaporizada por ', ' rompe a '
            ],
            [' devora a ', ' es aplastado por ', ' toma el sol con ',
                ' envenena a ', ' es decapitado por '
            ],
            [' es desautorizado por ', ' vaporiza a ', ' es envenenado por ',
                ' conversa con ', ' destroza a '
            ],
            [' cortan a ', ' son rotas por ', ' decapitan a ',
                ' son destrozadas por ', ' se cruzan con '
            ]
        ],
        PPTLS = function (choice1, choice2) {
            var index1 = options.indexOf(choice1),
                index2 = options.indexOf(choice2),
                dif = index2 - index1;
            if (dif < 0) {
                dif += options.length;
            }
            while (dif > 2) {
                dif -= 2;
            }
            return {
                result: results[index1][index2],
                diff: dif
            };
        },
        capitalize = function (string) {
            return string.charAt(0).toUpperCase() + string.slice(1);
        },
        play = function () {
            var ch1 = options[options.indexOf(this.id)],
                ch2 = options[Math.floor(Math.random() * 5)],
                res = PPTLS(ch1, ch2),
                resultPrefix = '';
            switch (res.diff) {
                case 0:
                    document.getElementById('ties').innerHTML = parseInt(
                        document.getElementById('ties').innerHTML) + 1;
                    resultPrefix = '<span class="text-warning">[EMPATAS]: ';
                    break;
                case 1:
                    document.getElementById('wins').innerHTML = parseInt(
                        document.getElementById('wins').innerHTML) + 1;
                    resultPrefix = '<span class="text-success">[GANAS]: ';
                    break;
                case 2:
                    document.getElementById('loses').innerHTML = parseInt(
                        document.getElementById('loses').innerHTML) + 1;
                    resultPrefix = '<span class="text-danger">[PIERDES]: ';
                    break;
            }
            var gameLog = document.getElementById('game-log');
            var textResult = capitalize(ch1) + res.result + ch2;
            var divResult = document.createElement('div');
            divResult.className = 'fadeInRight animated';
            divResult.innerHTML = resultPrefix + textResult + '.</span>';
            gameLog.insertBefore(divResult, gameLog.firstElementChild);
            console.log(textResult);
        },
        newGame = function () {
            document.getElementById('game-log').innerHTML = '';
            document.getElementById('wins').innerHTML = 0;
            document.getElementById('ties').innerHTML = 0;
            document.getElementById('loses').innerHTML = 0;
        },
        enterGame = function () {
            $('audio').animate({
                volume: 0
            }, 500);
            $('#intro-screen-container').one(
                'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',
                function () {
                    $(this).remove();
                });
            document.getElementById('intro-screen-container').className =
                'fadeOut animated';
        };
    var els = document.getElementById('play-buttons').getElementsByTagName(
        'button');
    for (var i = 0; i < els.length; i++) {
        els[i].addEventListener('click', play, false);
        $(els[i]).tooltip({
            container: 'body',
            html: true,
            trigger: 'hover'
        });
    }
    document.getElementById('new-game').addEventListener('click', newGame,
        false);
    document.getElementById('enter').addEventListener('click', enterGame, false);
});
